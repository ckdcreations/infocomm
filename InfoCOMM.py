#!/bin/python3

# ----------------------------------------------------------------------------
# Name: Clifford Dunn
# Description: InfoCOMM Script
# File Name: InfoCOMM.py
# Language: Python 3
# ----------------------------------------------------------------------------

# Important Libraries
import socket
import sys
import os
import simpleaudio
from ftplib import FTP


# ----------------------------------------------------------------------------
# Function: ftpFetcher
# Description: Connects to an FTP server and downloads a file
# Inputs: IP Address of FTP server, file name to pull
# Output: Downloads the file to C:\Temp
# ----------------------------------------------------------------------------
def ftpFetcher(IPaddress, filename):
    # Select the location for the remote file to be downloaded
    localFile = os.path.join(r"C:\Temp", filename)
    # PROPRIETARY INFORMATION: Root password for Symetrix device
    ftp = FTP(IPaddress, "root", "!#SymMARS")
    print('FTP Connected!')
    # Switch to the directory where the recorded audio lives and transfer
    ftp.cwd("/home/symetrix/audio")
    with open(localFile, 'wb') as f:
        ftp.retrbinary('RETR ' + filename, f.write)
        f.close()
    print('Transfer Complete!')
    # Close the connection
    ftp.close()

# ----------------------------------------------------------------------------
# Function: playFile 
# Description: Plays an audio file with the simpleaudio library
# Inputs: The name of the file to play
# Output: Plays the audio file through the default sound card.
# ----------------------------------------------------------------------------
def playFile(filename):
    print("Playing!")
    # Select the audio file from the C:\Temp folder
    wavFile = simpleaudio.WaveObject.from_wave_file(os.path.join(r"C:\Temp", filename))
    # Play the file and wait until it's finished
    playing = wavFile.play()
    return playing

def createDirectory(dirname):
    if os.path.exists(dirname) is False:
        os.mkdir(dirname)
            


# ----------------------------------------------------------------------------
# Function: Main
# Description: When triggered, downloads the audio file from Radius NX
#              and then plays it through the computer audio
# ----------------------------------------------------------------------------
if __name__ == "__main__":


    createDirectory("C:\Temp")

# Slight error checking to make sure there aren't too many arugments passed
    if int(len(sys.argv)) > 1: 
        print('Ignoring additional arguments...')
       

# Set up initial variables
    port = 55555
    addr = ('', port)

    filename = 'record.wav'
    loop = 0 
    audioChunk = 1024
    player = ''

        
# Create the socket for communication. Bind it to the local host and port 55555
    serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverSock.bind(addr)
    serverSock.listen(1)

# While loop that cycles through the process of receiving commands, connecting,
# downloading, and playing audio file.
    while loop == 0:

        print('Waiting for command...')

# Accept a connection to the client's socket. Receive the command.
        clientSock, clientAddr = serverSock.accept()
        print('connection from: ', clientAddr)
        data = clientSock.recv(64)
        print('data: ', data.decode())


# If the command transmitted is 'play', run the ftpFetcher and then playFile functions
        if data.decode() == 'play':
            ftpFetcher(clientAddr[0], filename)
            player = playFile(filename)
            #player.wait_done()
            #print('Play finished!')

# If the command transmitted is 'stop', stop the playing file.
        if data.decode() == 'stop':
            if type(player) != str and player.is_playing():
                player.stop()
            else:
                print('No file playing.')

# If the command transmitted is 'quit', quit the loop and end the script
        elif data.decode() == 'quit':
            print('Exiting Program!')
            loop = 1
            
# Close the connection to the client sockets. 
        clientSock.close()

